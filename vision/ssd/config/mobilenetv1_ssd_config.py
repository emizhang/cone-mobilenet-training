import numpy as np

from vision.utils.box_utils import SSDSpec, SSDBoxSizes, generate_ssd_priors


image_size = 416
image_mean = np.array([127, 127, 127])  # RGB layout
image_std = 128.0
iou_threshold = 0.45
center_variance = 0.1
size_variance = 0.2

specs = [
	SSDSpec(25, 17, SSDBoxSizes(20, 55), [2, 3]),
    SSDSpec(13, 32, SSDBoxSizes(105, 150), [2, 3]),
    SSDSpec(10, 42, SSDBoxSizes(150, 195), [2, 3]),
    SSDSpec(4, 104, SSDBoxSizes(195, 240), [2, 3]),
    SSDSpec(2, 208, SSDBoxSizes(240, 285), [2, 3]),
    SSDSpec(1, 416, SSDBoxSizes(285, 330), [2, 3])
    # SSDSpec(19, 16, SSDBoxSizes(60, 105), [2, 3]),
    # SSDSpec(10, 32, SSDBoxSizes(105, 150), [2, 3]),
    # SSDSpec(5, 64, SSDBoxSizes(150, 195), [2, 3]),
    # SSDSpec(3, 100, SSDBoxSizes(195, 240), [2, 3]),
    # SSDSpec(2, 150, SSDBoxSizes(240, 285), [2, 3]),
    # SSDSpec(1, 300, SSDBoxSizes(285, 330), [2, 3])
]


priors = generate_ssd_priors(specs, image_size)