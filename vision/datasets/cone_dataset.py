import numpy as np
from skimage import io
import urllib
import urllib.request
import pathlib
import xml.etree.ElementTree as ET
import csv
import cv2
import ast


class ConeDataset:

    def __init__(self, file, transform=None, target_transform=None, keep_difficult=False, is_test=False):
        """Dataset for driverless cone labeled data.
        Args:
            file: the root location of the cone datasets
        """
        self.file = file
        self.transform = transform
        self.target_transform = target_transform
        self.create_data_dict() #keys are locations of images, values are list bounding boxes [X0, Y0, H0, W0] for cones
        self.ids = list(self.data_dict.keys()) #ids are locations of images
        self.keep_difficult = keep_difficult


        self.class_names = ('BACKGROUND',
            'cone'
        )
        self.class_dict = {class_name: i for i, class_name in enumerate(self.class_names)}

    def __getitem__(self, index): #index 
        image_id = self.ids[index]
        boxes = self.data_dict[image_id]
        boxes, labels = self._get_annotation(image_id)
        image = self.url_to_image(image_id)
        # print(image)

        #pad images before resize to maintain aspect ratio
        h, w, _ = image.shape
        dim_diff = np.abs(h - w)
        pad1 = dim_diff // 2 #upper/left padding
        pad2 = dim_diff - (dim_diff // 2) #lower/right padding
        #determine if padding x or y axis 
        top, bottom, left, right = 0, 0, 0, 0
        if h<=w:
            top = pad1
            bottom = pad2
        else:
            left = pad1
            right = pad2

        image = cv2.copyMakeBorder(image, top, bottom, left, right, cv2.BORDER_CONSTANT, value=[128,128,128]) #gray border

        #resize to 400x400 and get boxes in terms of size ratios
        boxes = self.boxes_for_resize(boxes, self.cv_size(image), (top,bottom,left,right), (416,416))
        image = cv2.resize(image, (416,416))

        # # display boxes on image to confirm
        # display_image = image.copy()
        # for box in boxes:
        #     x1, y1, x2, y2 = box
        #     cv2.rectangle(display_image, (x1,y1), (x2,y2), (255,0,0), 1)
        # cv2.imshow("", display_image)
        # k = cv2.waitKey(0)
        # if k == 27:         # wait for ESC key to exit
        #     cv2.destroyAllWindows()


        # print("boxes before transform", boxes)
        # print("labels before transform", labels)

        if self.transform:
            image, boxes, labels = self.transform(image, boxes, labels)
        if self.target_transform:
            boxes, labels = self.target_transform(boxes, labels)

        # print("boxes AFTER transform", boxes)
        # print("labes AFTER", labels)

        return image, boxes, labels

    @staticmethod
    def boxes_for_resize(boxes, size, padding, new_size): #boxes from (x,y,h,w) to (x1,y1,x2,y2) ()
        width, height = size
        new_width, new_height = new_size
        top, bottom, left, right = padding

        boxes[:, 0] = (boxes[:, 0] + left) * new_width/width
        boxes[:, 1] = (boxes[:, 1] + bottom) * new_height/height
        temp_height = np.copy(boxes[:, 2])
        boxes[:, 2] = boxes[:, 0] + (new_width/width)*boxes[:, 3]
        boxes[:, 3] = boxes[:, 1] + (new_height/height)*temp_height
        return np.float32(boxes)


    #return image from url as original size
    @staticmethod 
    def url_to_image(url):
        # print("URL: ", url)
        resp = urllib.request.urlopen(url)
        image = np.asarray(bytearray(resp.read()), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        return image

    @staticmethod
    def cv_size(img):
        return tuple(img.shape[1::-1])

    def get_image(self, index):
        image_id = self.ids[index]
        image = self.url_to_image(image_id)
        if self.transform:
            image, _ = self.transform(image)
        return image

    def get_annotation(self, index):
        image_id = self.ids[index]
        return image_id, self._get_annotation(image_id)

    def __len__(self):
        return len(self.ids)

    # @staticmethod
    # def _read_image_ids(image_sets_file):
    #     ids = []
    #     with open(image_sets_file) as f:
    #         for line in f:
    #             ids.append(line.rstrip())
    #     return ids

    def create_data_dict(self):
        self.data_dict = {} # label is image name, value is list of bounding boxes 
        self.img_dict = {} #labe is image name 
        with open(self.file) as f:
            next(f) #skip first line
            csv_reader = csv.reader(f, delimiter='[')
            for row in csv_reader:
                image_loc = row[0].split(',')[1]
                boxes = []
                for i in range(1,len(row)): #skip image url
                    box_string = row[i]
                    boxes.append(ast.literal_eval("[" + row[i][:box_string.find(']')] + "]"))
                self.data_dict[image_loc] = boxes

    def _get_annotation(self, image_id):
        boxes = self.data_dict[image_id]
        labels = [self.class_dict['cone'] for i in range(len(boxes))]

        return (np.array(boxes, dtype=np.float32),
                np.array(labels, dtype=np.int64))





